set(test_exclusions)

if (WIN32)
  list(APPEND test_exclusions
    # Needs looked at.
    # Maybe https://gitlab.kitware.com/paraview/paraview/merge_requests/3728 fixes it?
    "ColorKnee")
endif ()

if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "paraview59")
  list(APPEND test_exclusions "OscillatorSingle;ModelSelection")
endif ()

string(REPLACE ";" "|" test_exclusions "${test_exclusions}")
if (test_exclusions)
  set(test_exclusions "(${test_exclusions})")
endif ()
