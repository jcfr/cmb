cmake_minimum_required(VERSION 3.12)

# In order to use a new superbuild, please copy the item from the date stamped
# directory into the `keep` directory. This ensure that new versions will not
# be removed by the uploader script and preserve them for debugging in the
# future.

set(data_host "https://data.kitware.com")

# Determine the tarball to download.
# 20220110 – Fix tests due to new operations and unsorted items.
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "vs2019")
  set(file_item "61dc7fb44acac99f42f25d88")
  set(file_hash "474d5a753220ec474257238d21ff8b95a4b824063b844f8ef83645556bcbb828cb0bfb137300d57fb0bbbdefce4514385b032dc5abe9c23ecb045d74d5267f2a")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos_arm64")
  set(file_item "61dc7fb34acac99f42f25d80")
  set(file_hash "adf86a84aba3e77b99911a5b2daee7154b4a026a7a8c92c75e1155a1dc14f2f009f5242a864680a0df6cf25e755479c4be2027ca5056f5b33f3918f72b52af93")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos_x86_64")
  set(file_item "61dc7fb44acac99f42f25d84")
  set(file_hash "751f476ece7a1ff7e6ffb38fcfe159906f9f7b7b24878827c0fcf7afaff0524bbfdbcf5983af5a41b564c9c86a893d73db806bf0cb2539c1592b0d312858d5d9")
else ()
  message(FATAL_ERROR
    "Unknown build to use for the superbuild")
endif ()

# Ensure we have a hash to verify.
if (NOT DEFINED file_item OR NOT DEFINED file_hash)
  message(FATAL_ERROR
    "Unknown file and hash for the superbuild")
endif ()

# Download the file.
file(DOWNLOAD
  "${data_host}/api/v1/item/${file_item}/download"
  ".gitlab/superbuild.tar.gz"
  STATUS download_status
  EXPECTED_HASH "SHA512=${file_hash}")

# Check the download status.
list(GET download_status 0 res)
if (res)
  list(GET download_status 1 err)
  message(FATAL_ERROR
    "Failed to download superbuild.tar.gz: ${err}")
endif ()

# Extract the file.
execute_process(
  COMMAND
    "${CMAKE_COMMAND}"
    -E tar
    xf ".gitlab/superbuild.tar.gz"
  RESULT_VARIABLE res
  ERROR_VARIABLE err
  ERROR_STRIP_TRAILING_WHITESPACE)
if (res)
  message(FATAL_ERROR
    "Failed to extract superbuild.tar.gz: ${err}")
endif ()
