# Linux-specific builder configurations and build commands

## Base images

### Fedora

.fedora33_paraview59:
    image: "kitware/cmb:ci-cmb-fedora33-paraview59-20220204"

    variables:
        GIT_SUBMODULE_STRATEGY: recursive
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci

.fedora33:
    image: "kitware/cmb:ci-cmb-fedora33-20220110"

    variables:
        GIT_SUBMODULE_STRATEGY: recursive
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci

### Lint builds

.fedora33_paraview59_compat:
    extends: .fedora33_paraview59

    variables:
        CMAKE_CONFIGURATION: fedora33_paraview59_compat
        # Allow warnings for backwards compatiblity
        CTEST_NO_WARNINGS_ALLOWED: 0

.fedora33_tidy:
    extends: .fedora33

    variables:
        CMAKE_CONFIGURATION: fedora33_tidy
        CTEST_NO_WARNINGS_ALLOWED: 1

.fedora33_memcheck:
    extends: .fedora33

    variables:
        CMAKE_BUILD_TYPE: RelWithDebInfo

.fedora33_asan:
    extends: .fedora33_memcheck

    variables:
        CMAKE_CONFIGURATION: fedora33_asan
        CTEST_MEMORYCHECK_TYPE: AddressSanitizer
        # Disable LeakSanitizer for now. It's catching all kinds of errors that
        # need investigated or suppressed.
        CTEST_MEMORYCHECK_SANITIZER_OPTIONS: detect_leaks=0

.fedora33_ubsan:
    extends: .fedora33_memcheck

    variables:
        CMAKE_CONFIGURATION: fedora33_ubsan
        CTEST_MEMORYCHECK_TYPE: UndefinedBehaviorSanitizer

.fedora33_coverage:
    extends: .fedora33

    variables:
        CMAKE_BUILD_TYPE: Debug
        CMAKE_CONFIGURATION: fedora33_coverage
        CTEST_COVERAGE: 1
        CMAKE_GENERATOR: Unix Makefiles

### Build and test

.fedora33_plain:
    extends: .fedora33

    variables:
        CMAKE_CONFIGURATION: fedora33_plain

## Tags

.linux_builder_tags:
    tags:
        - build
        - cmb
        - docker
        - linux

.linux_test_tags:
    tags:
        - cmb
        - docker
        - linux
        - x11

.linux_test_priv_tags:
    tags:
        - cmb
        - docker
        - linux
        - privileged
        - x11

## Linux-specific scripts

.before_script_linux: &before_script_linux
    - .gitlab/ci/cmake.sh
    - .gitlab/ci/ninja.sh
    - export PATH=$PWD/.gitlab:$PWD/.gitlab/cmake/bin:$PATH
    - cmake --version
    - ninja --version

.cmake_build_linux:
    stage: build

    script:
        - *before_script_linux
        - .gitlab/ci/sccache.sh
        - sccache --start-server
        - sccache --show-stats
        - "$LAUNCHER ctest -V -S .gitlab/ci/ctest_configure.cmake"
        - "$LAUNCHER ctest -V -S .gitlab/ci/ctest_build.cmake"
        - sccache --show-stats
    interruptible: true

.cmake_build_linux_tidy:
    stage: build

    script:
        - *before_script_linux
        - dnf install -y --setopt=install_weak_deps=False clang-tools-extra
        - .gitlab/ci/sccache.sh
        - sccache --start-server
        - sccache --show-stats
        - "$LAUNCHER ctest -V -S .gitlab/ci/ctest_configure.cmake"
        - "$LAUNCHER ctest -V -S .gitlab/ci/ctest_build.cmake"
        - sccache --show-stats
    interruptible: true

.cmake_test_linux:
    stage: test

    script:
        - *before_script_linux
        - "$LAUNCHER ctest --output-on-failure -V -S .gitlab/ci/ctest_test.cmake"
    interruptible: true

.cmake_memcheck_linux:
    stage: test

    script:
        - *before_script_linux
        - "$LAUNCHER ctest --output-on-failure -V -S .gitlab/ci/ctest_memcheck.cmake"
    interruptible: true

.cmake_coverage_linux:
    stage: analyze

    script:
        - *before_script_linux
        - "$LAUNCHER ctest --output-on-failure -V -S .gitlab/ci/ctest_coverage.cmake"
    coverage: '/Percentage Coverage: \d+.\d+%/'
    interruptible: true
