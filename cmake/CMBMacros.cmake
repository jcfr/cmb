# Some common macros used for configuring CMB Template application

set(_cmb_cmake_dir ${CMAKE_CURRENT_LIST_DIR})

# Function for setting defaults on CMB application global properties
function (cmb_set_property var value)
  cmake_parse_arguments(csp "FORCE" "" "" ${ARGN})

  get_property("_cmb_resource_set_${var}" GLOBAL PROPERTY "_cmb_application_${var}" SET)
  if (NOT _cmb_resource_set_${var} OR csp_FORCE)
    set_property(GLOBAL PROPERTY "_cmb_application_${var}" ${value})
  endif ()
endfunction ()

# Configure the About Dialog Reaction class
function (cmb_configure_about_dialog)
  cmake_parse_arguments(aboutdialog "" "CLASS_NAME" "SOURCES" ${ARGN})

  if (NOT aboutdialog_CLASS_NAME)
    message(FATAL_ERROR "cmb_configure_about_dialog: CLASS_NAME is required")
  endif ()

  if (NOT aboutdialog_SOURCES)
    message(FATAL_ERROR "cmb_configure_about_dialog: Must provide sources")
  endif ()

  set(source_list)
  foreach (source ${aboutdialog_SOURCES})
    file(REAL_PATH ${source} source_path)
    list(APPEND source_list ${source_path})
  endforeach ()

  cmb_set_property(aboutdialog_CLASS_NAME ${aboutdialog_CLASS_NAME} FORCE)
  cmb_set_property(aboutdialog_SOURCES ${source_list} FORCE)
endfunction ()

function (cmb_configure_layout_spec)
  cmake_parse_arguments(_cmb_layoutspec "" "CLASS_NAME" "SOURCES" ${ARGN})

  if (NOT _cmb_layoutspec_CLASS_NAME)
    message(FATAL_ERROR "cmb_configure_layout_spec: CLASS_NAME is required")
  endif ()

  if (NOT _cmb_layoutspec_SOURCES)
    message(FATAL_ERROR "cmb_configure_layout_spec: Must provide sources")
  endif ()

  set(source_list)
  foreach (source ${_cmb_layoutspec_SOURCES})
    file(REAL_PATH ${source} source_path)
    list(APPEND source_list ${source_path})
  endforeach ()

  configure_file(${_cmb_cmake_dir}/cmbLayoutSpec.h.in
                 ${cmb_BINARY_DIR}/cmbLayoutSpec.h
                 @ONLY)
  cmb_set_property(layoutspec_SOURCES ${source_list};${cmb_BINARY_DIR}/cmbLayoutSpec.h FORCE)

endfunction ()

# Function that configures all of the application variables
# If the application variable is not set, a default is computed
function (cmb_configure_properties)

  cmb_set_property(NAME "modelbuilder")
  get_property(_cmb_application_NAME GLOBAL PROPERTY _cmb_application_NAME)

  cmb_set_property(VERSION "${cmb_version}")
  get_property(_cmb_application_VERSION GLOBAL PROPERTY _cmb_application_VERSION)

  cmb_set_property(APP_NAME ${_cmb_application_NAME})
  get_property(_cmb_application_APP_NAME GLOBAL PROPERTY _cmb_application_APP_NAME)

  cmb_set_property(TITLE "${_cmb_application_APP_NAME} ${_cmb_application_VERSION} ${cmb_build_architecture}-bit")
  cmb_set_property(ORGANIZATION "Kitware Inc.")
  cmb_set_property(RESOURCE_DIR "${PROJECT_SOURCE_DIR}/modelbuilder/resource")


  # Get the info for configuring default resources
  get_property(_cmb_application_RESOURCE_DIR GLOBAL PROPERTY _cmb_application_RESOURCE_DIR)
  get_property(_cmb_application_NAME GLOBAL PROPERTY _cmb_application_NAME)

  # Default icon locations
  cmb_set_property(WINDOWS_ICON "${_cmb_application_RESOURCE_DIR}/win32/modelbuilder.ico")
  cmb_set_property(OSX_ICON "${_cmb_application_RESOURCE_DIR}/apple/modelbuilder.icns")
  cmb_set_property(LINUX_ICON_SIZES "22x22;32x32;96x96")
  get_property(_cmb_application_LINUX_ICON_SIZES GLOBAL PROPERTY _cmb_application_LINUX_ICON_SIZES)
  foreach (iconsize _cmb_application_LINUX_ICON_SIZES)
    cmb_set_property("LINUX_ICON_${iconsize}" "${_cmb_application_RESOURCE_DIR}/unix/modelbuilder-${iconsize}.png")
  endforeach ()
  cmb_set_property(APP_DATA "${_cmb_application_RESOURCE_DIR}/unix/modelbuilder.appdata.xml")
  cmb_set_property(ICON "${_cmb_application_RESOURCE_DIR}/modelbuilder-icon.png")
  cmb_set_property(SPLASH "${_cmb_application_RESOURCE_DIR}/modelbuilder-splash.png")

  cmb_set_property(QRESOURCE "")

  # Help Menu Links/Default about menu links
  cmb_set_property(ORGANIZATION_URL "https://www.kitware.com/")
  cmb_set_property(WEBSITE_URL "https://computationalmodelbuilder.org/")
  cmb_set_property(USER_GUIDE_URL "https://cmb.readthedocs.io/en/latest/")
  cmb_set_property(ISSUES_URL "https://gitlab.kitware.com/groups/cmb/-/issues")

endfunction ()
