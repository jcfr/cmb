//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "cmb/cmbObjectFactory.h"
#include "vtkVersion.h"

// Include all of the classes for which we want to create overrides.

#include "cmb/cmbReaderFactory.h"

vtkStandardNewMacro(cmbObjectFactory);

// Now create the functions to create overrides with.

VTK_CREATE_CREATE_FUNCTION(cmbReaderFactory);

cmbObjectFactory::cmbObjectFactory()
{
  this->RegisterOverride(
    "vtkSMReaderFactory",
    "cmbReaderFactory",
    "Override for ModelBuilder",
    1,
    vtkObjectFactoryCreatecmbReaderFactory);
}

const char* cmbObjectFactory::GetVTKSourceVersion()
{
  return VTK_SOURCE_VERSION;
}

void cmbObjectFactory::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
