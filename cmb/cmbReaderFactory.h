//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
/**
 * @class   cmbReaderFactory
 * @brief   is a factory for creating a reader
 * proxy based on the filename/extension.
 *
 * cmbReaderFactory is an extension of vtkSMReaderFactory to facilitate the
 * modification of the presented file reader list to filter non-SMTK readers
 * when post-processing mode is disabled.
*/
#ifndef modelbuilder_mbReaderFactory_h
#define modelbuilder_mbReaderFactory_h

#include "vtkSMReaderFactory.h"

class VTK_EXPORT cmbReaderFactory : public vtkSMReaderFactory
{
public:
  static cmbReaderFactory* New();
  vtkTypeMacro(cmbReaderFactory, vtkSMReaderFactory);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  cmbReaderFactory(const cmbReaderFactory&) = delete;
  cmbReaderFactory& operator=(const cmbReaderFactory&) = delete;

  /**
   * Toggle available readers to include ParaView's postprocessing readers.
   */
  void SetPostProcessingMode(bool choice);

  /**
   * Returns a formatted string with all supported file types.
   * \c cid is not used currently.
   * An example returned string would look like:
   * \verbatim
   * "Supported Files (*.vtk *.pvd);;PVD Files (*.pvd);;VTK Files (*.vtk)"
   * \endverbatim
   */
  const char* GetSupportedFileTypes(vtkSMSession* session) override;

protected:
  cmbReaderFactory();
  ~cmbReaderFactory() override;

  void UpdateAvailableReaders() override;

private:
  class vtkInternals;
  vtkInternals* Internals;
};

#endif
