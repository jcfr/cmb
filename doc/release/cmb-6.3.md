# CMB 6.3 Release Notes
CMB 6.3 is a minor release with new features.  **Note any non-backward compatible changes are in bold italics**. See also [CMB 6.2 Release Notes](cmb-6.2.md).


## CMB 6.3 Dependencies
* CMB 6.3 is based on SMTK 3.3 - For changes made to SMTK 3.3 please see the release notes in SMTK
* CMB 6.3 is based on a ParaView 5.6 variant
* CMB 6.3 is based on Qt 5.9 and supports Qt 5.13.1

## Support for Dark Mode
ModelBuilder can now support MacOS Dark Mode

## Support for Latest Attribute Resource File Formats
ModelBuilder can now read in Attribute Resources using the new 4.0 SBT/SMTK formats and now saves Attribute Resources using 4.0 SMTK files.

## Test Infrastructure Changes

+ ModelBuilder now names the default popup window so that tests
  which rely on the context menu showing toolbars and dock widgets
  will play back properly. Pre-existing tests that relied on
  the old name would not consistently play, but if any exist they
  need to be re-recorded or edited to reflect the new name.
