Animation Features
------------------

The ParaView animation features are now including in the postprocessing
plugin. When postprocessing is turned on, the Animation View is available
in the "View" menu, and a "Save Animation..." item is available in the
"File" menu.
