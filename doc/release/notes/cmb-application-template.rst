CMB Refactored to be a template SMTK application
------------------------------------------------

Application details such as icons and naming are now exposed via
CMake variables. The pqAboutDialogReaction and default LayoutSpec
may be overriden using application specific implementations.
