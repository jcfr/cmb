set(application_name "modelbuilder")

cmb_set_property(NAME ${application_name} FORCE)
cmb_set_property(APP_NAME ${application_name} FORCE)
cmb_set_property(TITLE "${application_name} ${cmb_version} ${cmb_build_architecture}-bit" FORCE)
cmb_set_property(ORGANIZATION "Kitware Inc." FORCE)
cmb_set_property(VERSION "${cmb_version}" FORCE)

set(resource_dir "${CMAKE_CURRENT_LIST_DIR}/resource")
cmb_set_property(RESOURCE_DIR "${resource_dir}" FORCE)

# Default icon locations
cmb_set_property(WINDOWS_ICON "${resource_dir}/win32/${application_name}.ico" FORCE)
cmb_set_property(OSX_ICON "${resource_dir}/apple/${application_name}.icns" FORCE)

cmb_set_property(LINUX_ICON_SIZES "22x22;32x32;96x96")
get_property(_cmb_application_LINUX_ICON_SIZES GLOBAL PROPERTY _cmb_application_LINUX_ICON_SIZES)
foreach (iconsize _cmb_application_LINUX_ICON_SIZES)
  cmb_set_property("LINUX_ICON_${iconsize}" "${resource_dir}/unix/${_cmb_application_NAME}-${iconsize}.png")
endforeach ()
# Only set this to override the default generated <name>.desktop file
#cmb_set_property(LINUX_DESKTOP "${resource_dir}/unix/${application_name}.desktop" FORCE)
cmb_set_property(APP_DATA "${resource_dir}/unix/${application_name}.appdata.xml" FORCE)

cmb_set_property(ICON "${resource_dir}/${application_name}-icon.png" FORCE)
cmb_set_property(SPLASH "${resource_dir}/${application_name}-splash.png" FORCE)

# Uncomment this to add application specific Qt resource files
#cmb_set_property(QRESOURCE "${resource_dir}/cmbResourceExtra.qrc")

# Help Menu Links/Default about menu links
cmb_set_property(ORGANIZATION_URL "https://www.kitware.com/" FORCE)
cmb_set_property(WEBSITE_URL "https://computationalmodelbuilder.org/" FORCE)
cmb_set_property(USER_GUIDE_URL "https://cmb.readthedocs.io/en/latest/" FORCE)
cmb_set_property(ISSUES_URL "https://gitlab.kitware.com/groups/cmb/-/issues" FORCE)

cmb_configure_about_dialog(
  CLASS_NAME mbAboutDialogReaction
  SOURCES
    mbAboutDialogReaction.cxx
    mbAboutDialogReaction.h)

cmb_configure_layout_spec(
  CLASS_NAME mbLayoutSpec
  SOURCES
    mbLayoutSpec.cxx
    mbLayoutSpec.h)
